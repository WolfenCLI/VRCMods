Changes:
 * FavCat: include canny link in-game
 * Finitizer: patch stuff differently to hopefully prevent even more floor poofing

**USE IT AT YOUR OWN RISK.** Modding the client is against VRChat ToS. I am not responsible for any bans or other punishments you may get by using these mods!